#include "funcoes.h"

//********************FUNÇÕES GENERICAS**********************************

int verificar_arquivo(char *nome_arquivo)
{//*
    FILE *arquivo = fopen(nome_arquivo,"r");

    if(arquivo == NULL)
    {
        return 0;
    }

    fseek(arquivo, 0, SEEK_END); //
    int tam_arquivo = ftell(arquivo);
    fclose(arquivo);
    //*/
    return tam_arquivo;
}

int get_opt(int argc,char **argv)
{//*
  int opt;

  while( (opt = getopt(argc, argv, "h,e,i:o")) > 0 )
  {
    switch ( opt )
    {
      case 'h': // opção -h
      help();
      return 0;

      case 'e': // opção -e
      erro();
      return 0;

      case 'i': // opção -i
      return 1;

      default:
      printf("Erro 1\n");
      return 0;

    }

  }

  //*/
  return 1;
}

void help()
{//*

  printf("-i <Nome do Arquivo de entrada>.txt -o <nome do arquivo de saida>.txt\t->\tPara executar normalmente\n");
  printf("-h\t\t\t\t->\tPara ajuda.\n");
  printf("-e\t\t\t\t->\tPara abrir os tipo de erros\n");

  //*/
  return;
}

void erro()
{//*

  printf("1: Erro no parametro enviado pelo usuario ao inicializar o programa\n");
  printf("100-x: Erros relacionado ao abrir arquivo ou ler cabeçalho\n");
  printf("\t1: Arquivo de entrada vazio ou não existe. Verificar o conteudo do Arquivo\n");
  printf("\t2: Arquivo de saida com problema. Verificar o conteudo do Arquivo\n");


  //*/
  return;
}

//*********************FUNÇÕES PRARTICULAR***************************
void desalocar(FILE *arquivo,FILE *arquivoSaida,Ponto *ponto,Dados *dados)
{//*
	free(ponto);
	free(dados);
	fclose(arquivo);
	fclose(arquivoSaida);
	//*/
	return;
}
