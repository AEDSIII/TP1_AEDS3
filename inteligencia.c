#include "funcoes.h"
#include "inteligencia.h"


void leituraAncora(FILE *arquivo, Dados *dados)
{//*

	fscanf(arquivo,"%d", &dados->quantidade);//pega a int do arquivo e coloca na variavel
	fscanf(arquivo,"%f", &dados->ancoraA);//pega a float do arquivo e coloca na variavel
	fscanf(arquivo,"%f", &dados->ancoraA);//pega a float do arquivo e coloca na variavel


	//*/
	return;
}

void leituraPontos(Dados *dados, Ponto *ponto, FILE *arquivo)
{//*
	int i;
	for(i = 0; i < dados->quantidade; i++)
	{
		fscanf(arquivo,"%d", &ponto[i].x);//pega a int do arquivo e coloca na variavel
		fscanf(arquivo,"%d", &ponto[i].y);//pega a int do arquivo e coloca na variavel
		ponto[i].colocacao = i;
		ponto[i].anguloA = 0;
		ponto[i].anguloB = 0;
		ponto[i].quantidadeInterna = 1	;
	}

	//*/
	return;
}

void calculaAngulo(Dados *dados,Ponto *ponto)
{//*
	int i;
	for(i = 0; i < dados->quantidade; i++)
	{
		ponto[i].anguloA = atan((ponto[i].x - dados->ancoraA )/ (ponto[i].y)  );
		ponto[i].anguloB = atan((ponto[i].x - dados->ancoraB )/ (ponto[i].y)  );

		if(ponto[i].anguloA < 0)
		{
			ponto[i].anguloA +=	3.14;
		}

		if(ponto[i].anguloB < 0)
		{
			ponto[i].anguloB +=	3.14;
		}
	}

	//*/
	return;
}

void verificador(Dados *dados, Ponto *ponto)
{//*
	int i,j;
	for(i = 0; i < dados->quantidade; i++)
	{
		for(j = i+1; j < dados->quantidade; j++)
		{

				if( (ponto[i].anguloA  <= ponto[j].anguloA ) && (ponto[i].anguloB >= ponto[j].anguloB) )
				{

					if( (ponto[i].quantidadeInterna >= ponto[j].quantidadeInterna))
					{

						ponto[i].quantidadeInterna = ++ponto[j].quantidadeInterna;


					}
				}
		}
	}


	return;
}

void resposta(Dados *dados,Ponto *ponto,FILE *arquivoSaida)
{//*
	int i, maior = 0;
	for(i = 0; i < dados->quantidade; i++)
	{
		if(ponto[i].quantidadeInterna > maior)
		{
			maior = ponto[i].quantidadeInterna;
		}
	}

	dados->resposta = maior;
	fprintf(arquivoSaida, "%d\n", dados->resposta);
	printf("\n\n%d %d\n",dados->quantidade, dados->resposta ); //dbug
	//*/
	return;
}
void Sort (int tamanho, Ponto *ponto, char tipo)
{
		int i, j;
		Ponto aux;
		for (j = 1; j < tamanho; ++j)
		{
				aux.colocacao = ponto[j].colocacao;
				aux.y = ponto[j].y;
				aux.x = ponto[j].x;
				if(tipo == 'x')
				{
					for (i = j-1; i >= 0 && ponto[i].x > aux.x; --i){
							ponto[i+1].colocacao = ponto[i].colocacao;
							ponto[i+1].y = ponto[i].y;
							ponto[i+1].x = ponto[i].x;

					}
				}else if(tipo == 'y') {
					for (i = j-1; i >= 0 && ponto[i].y > aux.y; --i){
							ponto[i+1].colocacao = ponto[i].colocacao;
							ponto[i+1].y = ponto[i].y;
							ponto[i+1].x = ponto[i].x;

					}
				}
				ponto[i+1].colocacao = aux.colocacao;
				ponto[i+1].y = aux.y;
				ponto[i+1].x = aux.x;
		}
}
