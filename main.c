/* Andre Felipe Jose Santos
 * Thiago Thadeu Souto Cardoso
 * Versao:2.0
 * Atualização:22/08/2018
 * Atualizado por:Thiago
*/
#include "funcoes.h"
#include "inteligencia.h"


int main(int argc, char *argv[])
{//*

	//Declaração de variaveis
	Dados *dados;
	Ponto *ponto;
	FILE *arquivo,*arquivoSaida;



	//Execução
	if( !get_opt(argc,argv) )
	{
		return 0;
	}

	if(!verificar_arquivo(argv[2]))
	{
		printf("Erro ao abrir o arquivo:100-1\n");
		return 0;
	}


	dados = malloc(1 * sizeof(Dados));

	arquivo=fopen(argv[2],"r");

	leituraAncora(arquivo, dados);

	ponto = malloc(dados->quantidade * sizeof(Ponto));
	leituraPontos(dados,ponto,arquivo);
	Sort (dados->quantidade, ponto, 'x');
	Sort (dados->quantidade, ponto, 'y');

	int i;
	for(i=0;i<dados->quantidade;i++)
		printf("%d %d\n", ponto[i].x, ponto[i].y );

	calculaAngulo(dados, ponto);
	verificador(dados, ponto);

	if(!verificar_arquivo(argv[2]))
	{
		printf("Erro ao abrir o Arquivo: 100-2\n");
		return 0;
	}
	arquivoSaida=fopen(argv[4],"wf");


	resposta(dados,ponto,arquivoSaida);



	desalocar(arquivo,arquivoSaida,ponto,dados);

	//*/
	return 0;
}
