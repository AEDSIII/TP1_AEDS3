#ifndef incruthia
#define incruthia

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#endif


#ifndef definicao
#define definicao

#define VAZIO -65
#define TAM 50

#endif
//-------------------------------/---------------------------------------

#ifndef strukt
#define strukt
//E definido as struct do codigo

typedef struct Dados{

	int quantidade;
	int resposta;
	float ancoraA;
	float ancoraB;



}Dados;

typedef struct Ponto{
	int colocacao;
	int x,y;
	float anguloA;
	float anguloB;
	int quantidadeInterna;
}Ponto;

#endif

//-------------------------------/---------------------------------------
#ifndef funcao
#define funcao

//********************FUNÇÕES GENERICAS**********************************

int verificar_arquivo(char* nome_arquivo);
/*Função para verifica se o arquivo está vazio.
 *
 *param: O nome do arquivo da imagem que deseja abrir
 *
 *return: 0 se o arquivo estiver vazio, ou qualquer outro valor se não.
*/

int get_opt(int argc,char **argv);
/*Função para o getopt.
 *
 *param: as primitivas do parametro do main.
 *
 *return: se é para continuar ou encerra o codigo.
*/

void help();
/*Função para imprimir o help para o usuario quando executar o codigo como ajuda.
 *
*/
void erro();
/*Função para imprimir os tipos de erro para o usuario quando executar o codigo como ajuda.
 *
*/
//*********************FUNÇÕES PRARTICULAR***************************

void desalocar(FILE *arquivo,FILE *arquivoSaida,Ponto *ponto,Dados *Dados);
/*Funcao para liberar memoria
 *
 *param: recebe todos os ponteiros que ainda nao foram liberados
*/

#endif
