#ifndef incruthia
#define incruthia

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#endif
//----------------------------------
#ifndef inteligenci
#define inteligenci

void leituraAncora(FILE *arquivo, Dados *dados);
/*Funcao para leitura das ancoras e a Dados de pontos no Arquivo
 *
 *param: recebe as ancoras e o nome do arquivo
*/

void leituraPontos(Dados *dados, Ponto *ponto, FILE *arquivo);
/*Funcao para ler os ṕontos do arquivo
 *
 *param: a Dados de pontos aonda e para guardar e o arquivo
*/

void calculaAngulo(Dados *dados, Ponto *ponto);
/*Funcao para calcular o angulo das retas.
 *
 *param: um vetor de struct de Ponto
*/

void verificador(Dados *dados, Ponto *ponto);
/*Funcao para verificar os pontos se cruzam.
 *
 *param: um vetor de struct de Ponto
*/

void resposta(Dados *dados,Ponto *ponto,FILE *arquivoSaida);
/*Funcao para verificar os pontos se cruzam.
 *
 *param: um vetor de struct de Ponto
*/

void Sort(int tamanho, Ponto *ponto, char tipo);
/*Funcao para ordena a entrada em orde crecente de pontos x e y dependendo da entrada.
 *
 *param: um vetor de struct de Ponto
*/
#endif
